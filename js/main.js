$(document).ready(function(){
	$('li.dropdown > a').on('click',function(event){
	    
	    event.preventDefault()
	    
	    $(this).parent().find('.menu-dropdown').first().toggle(300);
	    
	    $(this).parent().siblings().find('.menu-dropdown').hide(200);
	    
	    $(this).parent().find('.menu-dropdown').mouseleave(function(){  
	      var thisUI = $(this);
	      $('html').click(function(){
	        thisUI.hide();
	        $('html').unbind('click');
	      });
	    });
	});

	$('.btn-menu').on('click', function(e){
		e.preventDefault();
		if ($(this).hasClass("active")) {
			$("#navbar-main__wrapper").hide(300);
			$(this).removeClass("active");
		}else{
			$(this).addClass("active");
			$("#navbar-main__wrapper").toggle(300);
		}
	});
	 $('.accordion-toggle').on('click', function(event){
        event.preventDefault();
        // create accordion variables
        var accordion = $(this);
        var accordionContent = accordion.next('.accordion-content');

        accordion.toggleClass("open");
        accordionContent.slideToggle(250);

    });

	var length = $('#left').height() - $('#sidebar').height() + $('#left').offset().top;

	$(window).scroll(function () {

		var scroll = $(this).scrollTop();
		var height = $('#sidebar').height() + 'px';

		if (scroll < $('#left').offset().top) {

			$('#sidebar').css({
				'position': 'absolute',
				'top': '100'
			});

		} else if (scroll > length) {
			$('#left').css({
				'position': 'fixed',
				'bottom': '0',
				'margin-top': '500'
			});

		} else {
			$('#left').css({
				'position': 'absolute',
				'top': '0',
				'height': height
			});
		}
	});

});